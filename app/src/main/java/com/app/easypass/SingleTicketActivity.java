package com.app.easypass;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SingleTicketActivity extends AppCompatActivity {

    private Spinner transport;
    private EditText amount;
    private Button nextButton;
    private Button backButton;
    private HashMap<String,Double> transports_prices = new HashMap<>();
    private ArrayList<String> list = new ArrayList<>();
    private TextView price_single;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_ticket);
        fillTransportsPrices();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,list);
        transport = findViewById(R.id.single_transports);
        transport.setAdapter(adapter);
        price_single = findViewById(R.id.price_single);
        price_single.setText(" " +String.valueOf(transports_prices.get((String)transport.getSelectedItem()) ) + " "+ getText(R.string.euro_sign));
        amount = findViewById(R.id.single_amount);
        backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        nextButton = findViewById(R.id.next_button);
        transport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                price_single.setText(" " +String.valueOf(transports_prices.get((String)transport.getSelectedItem()) ) + " "+ getText(R.string.euro_sign));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selected_transport = (String) transport.getSelectedItem();
                if(TextUtils.isEmpty(amount.getText())){
                    amount.setError("You need to set a positive ammount!");
                    amount.requestFocus();
                }
                else{
                    int selected_amount = Integer.parseInt(amount.getText().toString());
                    attemptSingleTickets(selected_transport, selected_amount);
                }
            }
        });

    }

    private void fillTransportsPrices() {


        transports_prices.put("Boat",1.40);
        transports_prices.put("Fertagus",1.95);
        transports_prices.put("Lisbon Subway", 1.45);
        transports_prices.put("MTS",0.85);
        transports_prices.put("TST",3.40);
        for(Map.Entry<String,Double> entry : transports_prices.entrySet()){
            list.add(entry.getKey());
        }
    }

    private void attemptSingleTickets(String selected, int amount) {
        //VER SE AMMOUNT É SUPERIOR A 0
        if(amount <= 0 ){
            this.amount.setError("Amount needs to be positive!");
            this.amount.requestFocus();
        }
        else {
            Double total_pay = transports_prices.get(selected) * amount;
            //AQUI LANÇAREMOS A NOVA ATIVIDADE;
            Intent pay_Intent = new Intent(this,PaymentActivity.class);
            pay_Intent.putExtra("amount", total_pay);
            pay_Intent.putExtra("n_tickets", amount);
            pay_Intent.putExtra("name",selected);
            pay_Intent.putExtra("type","Single Ticket");
            startActivityForResult(pay_Intent,1);
        }


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 1){
            boolean no_funds = data.getBooleanExtra("no_funds",false);
            boolean success = data.getBooleanExtra("success",false);
            if(no_funds || success){
                setResult(RESULT_OK,data);
                finish();
            }


        }

    }
}
