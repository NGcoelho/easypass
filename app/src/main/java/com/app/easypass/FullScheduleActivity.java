package com.app.easypass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Arrays;


public class FullScheduleActivity extends AppCompatActivity {

    private Button backButton;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_schedule);
        backButton = findViewById(R.id.back_button);
        image = findViewById(R.id.full_schedule_image);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setImage();
    }

    private void setImage() {
        Intent intent = getIntent();
        String transport = intent.getStringExtra("transport");
        String route = intent.getStringExtra("route");
        setTitle(transport);
        switch (transport){
            case "Boat":
                switch (route){
                    case "Cacilhas - Cais do Sodré":
                        image.setImageResource(R.drawable.boat_cacilhas_cais);
                        break;
                    case "Barreiro - Terreiro do Paço":
                        image.setImageResource(R.drawable.boat_barreiro_terreiro);
                        break;
                }
            case "Fertagus":
                switch (route){
                    case "Sete-Rios (Destino Setúbal)" :
                        image.setImageResource(R.drawable.fertagus_sete_setubal);
                        break;
                    case "Coina (Destino Roma-Areeiro)":
                        image.setImageResource(R.drawable.fertagus_coina_roma);
                        break;
                }
                break;

            case "Lisbon Subway":
                switch (route){
                    case "Areeiro (Sentido Telheiras)" :
                        image.setImageResource(R.drawable.lisbonsub_areeiro_telheiras);
                        break;
                    case "Campo-Grande (Sentido Rato)":
                        image.setImageResource(R.drawable.lisbonsub_campo_rato);
                        break;
                }
                break;
        }
      /**  transports_routes.put("Boat", Arrays.asList("Cacilhas - Cais do Sodré","Barreiro - Terreiro do Paço"));
        transports_routes.put("Fertagus",Arrays.asList("Sete-Rios (Destino Setúbal)","Coina (Destino Roma-Areeiro)"));
        transports_routes.put("Lisbon Subway", Arrays.asList("Areeiro (Sentido Telheiras)","Campo-Grande (Sentido Rato)")); **/

       /** PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(image);
        pAttacher.update();**/
    }
}
