package com.app.easypass;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TimetablesFragment extends Fragment {

    private Spinner transportSpinner;
    private Spinner routeSpinner;
    private ImageView short_timetable;
    private HashMap<String, List<String>> transports_routes = new HashMap<>();
    private Context context;
    private Button full_Schedule;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_timetables,container,false);
       fill_map();
       context = getContext();
       transportSpinner = view.findViewById(R.id.transport);
       full_Schedule = view.findViewById(R.id.next_button);

       routeSpinner = view.findViewById(R.id.route);
       short_timetable = view.findViewById(R.id.short_timetable);
       final String[] dataSpinner1 = new String[transports_routes.keySet().size()];
        transports_routes.keySet().toArray(dataSpinner1);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(view.getContext(),R.layout.support_simple_spinner_dropdown_item,dataSpinner1);
        transportSpinner.setAdapter(adapter1);
        transportSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<String> routes = transports_routes.get(dataSpinner1[position]);
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(context,R.layout.support_simple_spinner_dropdown_item,routes);
                routeSpinner.setAdapter(adapter2);
                String transport = (String) transportSpinner.getSelectedItem();
                String route = (String) routeSpinner.getSelectedItem();
                updateImage(transport,route);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Nada a fazer
            }
        });

        routeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String transport = (String) transportSpinner.getSelectedItem();
                String route = (String) routeSpinner.getSelectedItem();
                updateImage(transport,route);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        full_Schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String transport = (String) transportSpinner.getSelectedItem();
                String route = (String) routeSpinner.getSelectedItem();
                Intent newIntent = new Intent(getContext(), FullScheduleActivity.class);
                newIntent.putExtra("transport",transport);
                newIntent.putExtra("route",route);
                startActivity(newIntent);
            }
        });


       return view;


    }

    private void updateImage(String transport, String route){
        switch (transport) {
            case "Boat":

                switch (route){
                    case "Cacilhas - Cais do Sodré" :
                        short_timetable.setImageResource(R.drawable.boat_cacilhas_c_shrot);
                        break;
                    case "Barreiro - Terreiro do Paço":
                        short_timetable.setImageResource(R.drawable.boat_bairreiro_t_short);
                        break;
                }
                break;

            case "Fertagus":
                switch (route){
                    case "Sete-Rios (Destino Setúbal)" :
                        short_timetable.setImageResource(R.drawable.fertagus_sete_setubal_short);
                        break;
                    case "Coina (Destino Roma-Areeiro)":
                        short_timetable.setImageResource(R.drawable.fertagus_coina_roma_short);
                        break;
                }
                break;

            case "Lisbon Subway":
                switch (route){
                    case "Areeiro (Sentido Telheiras)" :
                        short_timetable.setImageResource(R.drawable.lisbonsub_areeiro_telheiras_short);
                        break;
                    case "Campo-Grande (Sentido Rato)":
                        short_timetable.setImageResource(R.drawable.lisbonsub_campo_rato_short);
                        break;
                }
                break;
        }
    }

    private void fill_map() {
        transports_routes.put("Boat", Arrays.asList("Cacilhas - Cais do Sodré","Barreiro - Terreiro do Paço"));
        transports_routes.put("Fertagus",Arrays.asList("Sete-Rios (Destino Setúbal)","Coina (Destino Roma-Areeiro)"));
        transports_routes.put("Lisbon Subway", Arrays.asList("Areeiro (Sentido Telheiras)","Campo-Grande (Sentido Rato)"));


    }

}
