package com.app.easypass;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.bottomnavigation.LabelVisibilityMode;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Context mContext;
    private final MyWalletFragment walletFragment = new MyWalletFragment();
    private final MyPassesFragment passesFragment = new MyPassesFragment();
    private final BuyPassesFragment buyPassesFragment = new BuyPassesFragment();
    private final TimetablesFragment timetablesFragment = new TimetablesFragment();
    private Fragment active = passesFragment;
    private FragmentManager fm = getSupportFragmentManager();
    private  BottomNavigationView navigation;
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment;
            switch (item.getItemId()) {
                case R.id.navigation_my_tickets:
                        fm.beginTransaction().hide(active).show(passesFragment)
                                .commit();
                        active = passesFragment;
                    setTitle("My Passes");
                    break;
                case R.id.navigation_timetables:
                    fm.beginTransaction().hide(active).show(timetablesFragment)
                            .commit();
                    active = timetablesFragment;
                    setTitle("Timetables");
                    break;
                case R.id.navigation_wallet:
                    fm.beginTransaction().hide(active).show(walletFragment)
                            .commit();
                    active = walletFragment;
                    setTitle("My Wallet");
                    break;
                case R.id.navigation_buy_tickets:
                    fm.beginTransaction().hide(active).show(buyPassesFragment)
                            .commit();
                    active = buyPassesFragment;
                    setTitle("Buy Passes");
                    break;

                 default: return false;
            }

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext=this;

        fm.beginTransaction().add(R.id.main_container,timetablesFragment,"4")
                .hide(timetablesFragment).commit();
        fm.beginTransaction().add(R.id.main_container,buyPassesFragment,"3")
                .hide(buyPassesFragment).commit();
        fm.beginTransaction().add(R.id.main_container,walletFragment,"2")
                .hide(walletFragment).commit();
        fm.beginTransaction().add(R.id.main_container,passesFragment,"1")
                .commit();
        setTitle("My Passes");



        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);

        if(getIntent().getBooleanExtra("fromRegistry",false) == true){
            AlertDialog.Builder builder;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                builder  = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
            else builder = new AlertDialog.Builder(mContext);
            builder.setTitle(R.string.welcome)
                    .setMessage(R.string.prompt_funds)
                    .setPositiveButton(R.string.my_wallet_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Here we'll go to the funds/wallet fragment
                            fm.beginTransaction().hide(active).show(walletFragment).commit();
                            navigation.getMenu().getItem(1).setChecked(true);
                            active=walletFragment;
                        }
                    })
                    .setNegativeButton(R.string.dismiss_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(R.drawable.easypass_logo_transparent)
                    .show();
        }
        else{
            AlertDialog.Builder builder;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                builder  = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
            else builder = new AlertDialog.Builder(mContext);
            builder.setTitle(R.string.welcome_back)
                    .setMessage("")
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(R.drawable.easypass_logo_transparent)
                    .show();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fm.beginTransaction().detach(passesFragment).attach(passesFragment).commit();
        fm.beginTransaction().detach(walletFragment).attach(walletFragment).commit();
        fm.beginTransaction().detach(buyPassesFragment).attach(buyPassesFragment).commit();
        fm.beginTransaction().detach(timetablesFragment).attach(timetablesFragment).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 1){
            boolean no_funds = data.getBooleanExtra("no_funds",false);
            if(no_funds){
                fm.beginTransaction().hide(active).show(walletFragment).commit();
                active = walletFragment;
                navigation.getMenu().getItem(1).setChecked(true);

            }
            boolean success = data.getBooleanExtra("success", false);
            if(success){
                passesFragment.changeData();
                fm.beginTransaction().hide(active).show(passesFragment).commit();
                active = passesFragment;
                navigation.getMenu().getItem(0).setChecked(true);
            }
        }

    }




}
