package com.app.easypass;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.easypass.R;

import java.text.NumberFormat;

public class BuyPassesFragment extends Fragment {

    private Button mMonthlyPassesButton;
    private Button mSingleTicketButton;
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_buy_passes,container,false);
        mContext = inflatedView.getContext();
        mMonthlyPassesButton = inflatedView.findViewById(R.id.monthly_pass_button);
        mSingleTicketButton = inflatedView.findViewById(R.id.single_pass_button);
        mMonthlyPassesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,BuyMonthlyPassesActivity.class);
                getActivity().startActivityForResult(intent,1);
            }
        });
        mSingleTicketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,SingleTicketActivity.class);
                getActivity().startActivityForResult(intent,1);
            }
        });

        return inflatedView;
    }


}
