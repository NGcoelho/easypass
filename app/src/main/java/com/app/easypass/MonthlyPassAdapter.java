package com.app.easypass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

public class MonthlyPassAdapter extends BaseAdapter {

    private ArrayList<Map.Entry<String,Double>> list ;
    private Context context;

    public MonthlyPassAdapter(ArrayList<Map.Entry<String,Double>> list, Context context){
        this.list=list;
        this.context=context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.monthly_passes_layout,null);
            Map.Entry<String,Double> entry = (Map.Entry<String,Double>) getItem(position);
            TextView monthly_pass_name = view.findViewById(R.id.monthly_pass_name);
            monthly_pass_name.setText(entry.getKey());
            TextView monthly_pass_value = view.findViewById(R.id.monthly_pass_value);
            monthly_pass_value.setText(String.valueOf(entry.getValue())+view.getResources().getString(R.string.euro_sign));
            Button buy_button = (Button) view.findViewById(R.id.buy_button);
            buy_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent pay_Intent = new Intent(context,PaymentActivity.class);
                    pay_Intent.putExtra("amount", list.get(position).getValue());
                    pay_Intent.putExtra("name",list.get(position).getKey());
                    pay_Intent.putExtra("type","Monthly Pass");
                    ((Activity)context).startActivityForResult(pay_Intent,1);
                }
            });
        }
        return view;
    }
}
