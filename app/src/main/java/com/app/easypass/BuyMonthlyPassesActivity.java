package com.app.easypass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BuyMonthlyPassesActivity extends AppCompatActivity {

    private ListView mListView;
    private Button mBackButton;
    private ArrayList<Map.Entry<String,Double>> list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_monthly_passes);
        populateList();
        mListView = findViewById(R.id.monthly_passes_list);
        mBackButton = findViewById(R.id.back_button);
        mListView.setAdapter(new MonthlyPassAdapter(list,this));
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void populateList() {
      HashMap<String, Double> monthPassesMap = new HashMap<>();
      //Pragal-Lisboa
      monthPassesMap.put("Fertagus Train",43.65);
      monthPassesMap.put("MTS Subway",16.75);
      monthPassesMap.put("Lisbon Subway",43.25);
      monthPassesMap.put("Boat",21.20);
      monthPassesMap.put("Lisbon Subway + TST",45.70);
      monthPassesMap.put("Fertagus Train + Lisbon Subway",36.70);

      for(Map.Entry<String,Double> entry : monthPassesMap.entrySet()){
          list.add(entry);
      }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 1){
            boolean no_funds = data.getBooleanExtra("no_funds",false);
            boolean success = data.getBooleanExtra("success",false);
            if(no_funds || success){
                setResult(RESULT_OK,data);
                finish();
            }


        }

    }
}
