package com.app.easypass;

import java.io.Serializable;
import java.util.HashMap;

public class User implements Serializable {
    private String fname;
    private String uname;
    private String birthday;
    private String email;
    private String password;
    private Double balance;
    private HashMap<String,Pass> userPasses = new HashMap<>();
    static final long serialVersionUID = 1L;

    public User(String fname, String uname, String birthday, String email, String password) {
        this.fname = fname;
        this.uname = uname;
        this.birthday = birthday;
        this.email = email;
        this.password = password;
        this.balance=0.0;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public String getFname() {
        return fname;
    }

    public String getPassword() {
        return password;
    }

    public String getUname() {
        return uname;
    }

    public Double getBalance() {return balance;}

    public void setBalance(double ammount){
        this.balance+=ammount;
    }
    public void addPass(Pass pass){
        userPasses.put(pass.getName(),pass);
    }

    public HashMap<String, Pass> getUserPasses() {
        return userPasses;
    }

    @Override
    public String toString() {
        return email+"; "+fname+"; "+uname+"; "+birthday;
    }
}
