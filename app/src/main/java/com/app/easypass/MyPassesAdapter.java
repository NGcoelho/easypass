package com.app.easypass;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

public class MyPassesAdapter extends BaseAdapter {

    private ArrayList<Map.Entry<String,Pass>> list ;
    private Context context;
    public MyPassesAdapter(ArrayList<Map.Entry<String,Pass>> list, Context context){
        this.list=list;
        this.context=context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        Pass pass = list.get(position).getValue();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.my_passes_layout,parent,false);
            Button qrButton = view.findViewById(R.id.qr_button);
            qrButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //IR BUSCAR QR CODE
                    Intent newIntent = new Intent(context,QRActivity.class);
                    context.startActivity(newIntent);
                }
            });
            TextView pass_type = view.findViewById(R.id.pass_type);
            TextView pass_name = view.findViewById(R.id.pass_name);
            TextView pass_expiration = view.findViewById(R.id.pass_expiration);
            pass_type.setText(pass.getType());
            pass_name.setText(pass.getName());
            pass_expiration.setText(pass.getExpiration());
        }
        else view = convertView;



        return view;
    }
}
