package com.app.easypass;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.HashMap;

public class MyWalletFragment extends Fragment {
    private TextView balance;
    private Button history_button;
    private Button funds_button;
    private  Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.fragment_my_wallet,container,false);
        mContext = inflatedView.getContext();
        balance = inflatedView.findViewById(R.id.money_balance);
        history_button = inflatedView.findViewById(R.id.purchase_history_button);
        funds_button = inflatedView.findViewById(R.id.add_funds_button);
        history_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"WORKING", Toast.LENGTH_SHORT ).show();
            }
        });
        funds_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(mContext,FundsActivity.class);
                startActivityForResult(newIntent,1);

            }
        });

        //ir buscar o dinheiro do utilizador
        //implica ir buscar o mapa e o active user
        //buscar o guito ao user e fazer set Text com isso( underlined mb)
        Gson gson = new Gson();
        SharedPreferences preferences = getActivity().getSharedPreferences("Users",0);
        //CURRENTUSER A NULL ENQUANTO COMEÇARMOS APLICAÇÃO NO MAIN ACTIVITY
        String active_user = preferences.getString("currentUser", "nuno@mail.com");
        String string_map = preferences.getString("userMap", null);
        Type type = new TypeToken<HashMap<String, User>>() {
        }.getType();
        HashMap<String,User> usersMap = gson.fromJson(string_map, type);
        Double amount = usersMap.get(active_user).getBalance();
        NumberFormat format = NumberFormat.getCurrencyInstance();
        SpannableString content = new SpannableString(format.format(amount));
        content.setSpan(new UnderlineSpan(),0,content.length(),0);
        balance.setText(content);


        //se calhar inflater tem de vir antes
        return inflatedView;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == getActivity().RESULT_OK && requestCode == 1){
            Double newAmount =  data.getDoubleExtra("newBalance",-1000);
            NumberFormat format = NumberFormat.getCurrencyInstance();
            SpannableString content = new SpannableString(format.format(newAmount));
            content.setSpan(new UnderlineSpan(),0,content.length(),0);
            balance.setText(content);
        }

    }
}
