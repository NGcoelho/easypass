package com.app.easypass;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

public class FundsActivity extends AppCompatActivity {

    private EditText mAmount;
    private Spinner mMethod;
    private Button add_button;
    private Button back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds);
        mAmount = findViewById(R.id.amount_to_add);
        mMethod =  findViewById(R.id.pay_method);
        add_button = findViewById(R.id.add_button);
        back_button = findViewById(R.id.back_button);
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_funds();
            }
        });

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String[] items = new String[]{getString(R.string.prompt_pay_method), "MBWay","Credit Card","Paypal"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,items);
        mMethod.setAdapter(adapter);

    }

    private void add_funds() {

        mAmount.setError(null);
        String s_amount = mAmount.getText().toString();
        if(TextUtils.isEmpty(s_amount) || Integer.parseInt(s_amount) == 0 ){
            mAmount.setError(getString(R.string.error_add_funds));
            mAmount.requestFocus();
        }
        else if (Integer.parseInt(s_amount) > 1000){
            mAmount.setError(getString(R.string.error_much_funds));
            mAmount.requestFocus();
        }
        else if(mMethod.getSelectedItem().toString().equals(getString(R.string.prompt_pay_method))){
            Toast.makeText(this,getString(R.string.error_payment_method),Toast.LENGTH_SHORT).show();
            mMethod.requestFocus();
        }
        else{
            Double amount = Double.parseDouble(s_amount);
            Gson gson = new Gson();
            SharedPreferences preferences = getSharedPreferences("Users",0);
            String active_user = preferences.getString("currentUser", "nuno@mail.com");
            String string_map = preferences.getString("userMap", null);
            Type type = new TypeToken<HashMap<String, User>>() {
            }.getType();
            HashMap<String,User> usersMap = gson.fromJson(string_map, type);
            User current = usersMap.get(active_user);
            current.setBalance(amount);
            usersMap.put(active_user,current);
            SharedPreferences.Editor editor = preferences.edit();
            string_map = gson.toJson(usersMap);
            editor.putString("userMap",string_map);
            editor.commit();
            Toast.makeText(this, "Funds added!",Toast.LENGTH_SHORT).show();
            Intent resultIntent = new Intent();
            resultIntent.putExtra("newBalance",current.getBalance());
            setResult(RESULT_OK,resultIntent);
            finish();

        }



    }
}
