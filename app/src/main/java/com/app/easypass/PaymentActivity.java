package com.app.easypass;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.HashMap;

public class PaymentActivity extends AppCompatActivity {

    private TextView mValueToPay;
    private Button payButton;
    private Button backButton;
    private Intent amount_Intent;
    private Context mContext;
    Boolean cancel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=this;
        setContentView(R.layout.activity_payment);
        mValueToPay = findViewById(R.id.value_to_pay);
        amount_Intent = getIntent();
        final Double amount = amount_Intent.getDoubleExtra("amount",-1000);
        final String name = amount_Intent.getStringExtra("name");
        final String type = amount_Intent.getStringExtra("type");
        NumberFormat format = NumberFormat.getCurrencyInstance();
        SpannableString content = new SpannableString(format.format(amount));
        content.setSpan(new UnderlineSpan(),0,content.length(),0);
        mValueToPay.setText(content);
        payButton = findViewById(R.id.pay_button);
        backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_Alert);
                else builder = new AlertDialog.Builder(mContext);
                builder.setTitle(R.string.confirm_op)
                        .setMessage(R.string.confirm_operation)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                cancel = false;
                                attemptPay(name, amount,type);
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                cancel = true;
                                dialog.cancel();
                            }
                        })
                        .setIcon(R.drawable.easypass_logo_transparent)
                        .show();

            }
        });
    }

    private void attemptPay(String name,Double pay_amount,String s_type) {
        Gson gson = new Gson();
        SharedPreferences preferences = getSharedPreferences("Users",0);
        String active_user = preferences.getString("currentUser", "nuno@mail.com");
        String string_map = preferences.getString("userMap", null);
        Type type = new TypeToken<HashMap<String, User>>() {
        }.getType();
        HashMap<String,User> usersMap = gson.fromJson(string_map, type);
        User current = usersMap.get(active_user);
        if(current.getBalance() < pay_amount){
            AlertDialog.Builder builder;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                builder  = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            else builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.error)
                    .setMessage(R.string.no_funds)
                    .setPositiveButton(getString(R.string.go_to)+" "+getString(R.string.my_wallet_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Here we'll go to the funds/wallet fragment
                            Intent return_intent = new Intent();
                            return_intent.putExtra("no_funds",true);
                            setResult(RESULT_OK,return_intent);
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.dismiss_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .show();
        }
        else if (!cancel) {
                current.setBalance(-pay_amount);
                String full_name = "";
                if (s_type.equals("Monthly Pass")) {
                    switch (name) {
                        case "Fertagus Train + Lisbon Subway":
                            full_name = "Fertagus Pragal - Roma-Areeiro + Lisbon Subway";
                            break;
                        case "Fertagus Train":
                            full_name = "Fertagus Sete-Rios - Coina";
                            break;
                        case "MTS Subway":
                            full_name = name + " Pass";
                            break;
                        case "Lisbon Subway":
                            full_name = "Navegante (Lisbon Subway)";
                            break;
                        case "Boat":
                            full_name = "Boat Cais do Sodré - Cacilhas";
                            break;
                        case "Lisbon Subway + TST":
                            full_name = "Navegante (Lisbon Subway) + TST";
                    }
                    current.addPass(new Pass(s_type, full_name, "31/12/18"));
                } else if (s_type.equals("Single Ticket")) {
                    int numberOfTickets = amount_Intent.getIntExtra("n_tickets", -1000);
                    full_name = name + " Single Ticket";
                    current.addPass(new Pass(s_type, full_name, "Amount: " + numberOfTickets));
                }

                usersMap.put(current.getEmail(), current);
                string_map = gson.toJson(usersMap);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("userMap", string_map);
                editor.commit();
                Intent return_intent = new Intent();
                return_intent.putExtra("success", true);
                setResult(RESULT_OK, return_intent);
                AlertDialog.Builder builder1;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    builder1  = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
                else builder1 = new AlertDialog.Builder(this);
                builder1.setTitle(R.string.purchase_ok)
                        .setMessage("")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        })
                        .show();
        }
    }

}
