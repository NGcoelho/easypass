package com.app.easypass;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class MyPassesFragment extends Fragment {

    private ListView passesList;
    private ArrayList<Map.Entry<String,Pass>> list=new ArrayList<>();
    private MyPassesAdapter adapter;
    private TextView emptyView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_passes,container,false);
        passesList = view.findViewById(R.id.my_passes_list);
        emptyView = view.findViewById(R.id.empty_list);
        Gson gson = new Gson();

        SharedPreferences preferences = getActivity().getSharedPreferences("Users",0);
        String active_user = preferences.getString("currentUser", "nuno@mail.com");
        String string_map = preferences.getString("userMap", null);
        Type type = new TypeToken<HashMap<String, User>>() {
        }.getType();
        HashMap<String,User> usersMap = gson.fromJson(string_map, type);
        HashMap<String,Pass> userPasses = usersMap.get(active_user).getUserPasses();
        populateList(userPasses);
        adapter = new MyPassesAdapter(list,getContext());
        passesList.setEmptyView(emptyView);
        passesList.setAdapter(adapter);

        return view;

        //TEMOS DE IR BUSCAR PASSES DO UTILLIZADOR METE LOS NUMA LIST VIEW E ALERTAR O ADAPTER
    }

    private void populateList(HashMap<String,Pass> userPasses) {
        Iterator<Map.Entry<String,Pass>> iterator;
        boolean dont;
        List<Map.Entry<String,Pass>> singleTickets = new LinkedList<>(), MonthlyPasses= new LinkedList<>();

        for(Map.Entry<String,Pass> entry : userPasses.entrySet()){
            if(entry.getValue().getType().equals("Monthly Pass"))
                MonthlyPasses.add(entry);
            else singleTickets.add(entry);

        }
        //SINGLE TICKETS FIRST
        for(Map.Entry<String,Pass> entry : singleTickets){
            dont = false;
            iterator = list.iterator();
            while(iterator.hasNext()){
                Map.Entry<String,Pass> next = iterator.next();
                if(next.getKey().equals(entry.getKey())) dont = true;
            }
            if(!dont)
                list.add(entry);
        }
        //MONTHLY PASSES THEN
        for(Map.Entry<String,Pass> entry : MonthlyPasses){
            dont = false;
            iterator = list.iterator();
            while(iterator.hasNext()){
                Map.Entry<String,Pass> next = iterator.next();
                if(next.getKey().equals(entry.getKey())) dont = true;
            }
            if(!dont)
                list.add(entry);
        }




    }
    public void changeData(){
        adapter.notifyDataSetChanged();
    }
}
