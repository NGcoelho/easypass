package com.app.easypass;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity {
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        final Button login_button = findViewById(R.id.login_button);
        final Button register_button = findViewById(R.id.register_button);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(StartActivity.this, "LOGIN FUNCIONA! YES", Toast.LENGTH_SHORT).show();
                Intent login_intent = new Intent(context,LoginActivity.class);
                startActivity(login_intent);
            }
        });

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(StartActivity.this, "REGISTER FUNCIONA! YES", Toast.LENGTH_SHORT).show();
                Intent register_intent = new Intent(context,RegisterActivity.class);
                startActivity(register_intent);
            }
        });
    }
}
