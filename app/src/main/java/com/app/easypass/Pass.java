package com.app.easypass;

public class Pass {
    private String type;
    private String name;
    private String expiration;

    public Pass(String type, String name, String expiration){
        this.type=type;
        this.name=name;
        this.expiration=expiration;
    }

    public String getExpiration() {
        return expiration;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
